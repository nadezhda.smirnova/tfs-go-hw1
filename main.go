package main

import (
	"encoding/csv"
	"fmt"
	"math/big"
	"os"
	"strconv"

	"github.com/pkg/errors"
)

type Candle struct {
	max float64
	min float64
	ts  string
	tb  string
}

func readSample(name string) ([][]string, error) {
	file, err := os.Open(name) 
	if err != nil {
		return nil, errors.Wrap(err, "open failed")
	}
	defer file.Close()

	fileReader := csv.NewReader(file)

	rows, err := fileReader.ReadAll()
	if err != nil {
		return nil, errors.Wrap(err, "read failed")
	}

	return rows, err
}
func mapUser(rows [][]string) map[string]map[string]*big.Float {
	var d *big.Float

	u := make(map[string]map[string]*big.Float)
	length := len(rows)

	for i := 0; i < length; i++ {
		id := rows[i][0]
		ticker := rows[i][2]

		if u[id] != nil {
			if u[id][ticker] == nil {
				d = big.NewFloat(0)
			} else {
				d = u[id][ticker]
			}

			u[id][ticker] = new(big.Float).Add(delta(rows[i][3], rows[i][4]), d)
		} else {
			mapInit := make(map[string]*big.Float)
			mapInit[ticker] = delta(rows[i][3], rows[i][4])
			u[id] = mapInit
		}
	}

	return u
}
func string2float(str string) float64 {
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {
		fmt.Println(errors.Wrap(err, "converting string to float64 failed"))
	}

	return f
}
func delta(str1, str2 string) *big.Float {
	f1 := big.NewFloat(string2float(str1))
	f2 := big.NewFloat(string2float(str2))

	return new(big.Float).Sub(f2, f1)
}
func mapTicker(rows [][]string) map[string]Candle {
	var c, cn Candle

	tc := make(map[string]Candle)
	length := len(rows)

	for i := 0; i < length; i++ {
		t := rows[i][0]
		c.max = string2float(rows[i][3])
		c.min = string2float(rows[i][4])
		c.tb = rows[i][1]
		c.ts = rows[i][1]

		if tc[t].tb != "" {
			if tc[t].max < c.max {
				cn = tc[t]
				cn.max = c.max
				cn.ts = c.ts
				tc[t] = cn
			}

			if tc[t].min > c.min {
				cn = tc[t]
				cn.min = c.min
				cn.tb = c.tb
				tc[t] = cn
			}
		} else {
			tc[t] = c
		}
	}

	return tc
}
func printOutput(mapU map[string]map[string]*big.Float, mapT map[string]Candle) {
	var result [][]string

	f, err := os.OpenFile("output.csv", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(errors.Wrap(err, "open failed"))
	}

	for id := range mapU {
		for tic := range mapU[id] {
			var rID []string
			//Append id user and ticker to result(id).
			rID = append(rID, id, tic)
			//Append user revenue to result(id).
			rID = append(rID, mapU[id][tic].String())
			//Calculate and append max revenue to result(id).
			mx := big.NewFloat(mapT[tic].max)
			mn := big.NewFloat(mapT[tic].min)
			mRev := new(big.Float).Sub(mx, mn)
			rID = append(rID, mRev.String())
			//Append diff revenue to result(id).
			diff := new(big.Float).Sub(mRev, mapU[id][tic])
			rID = append(rID, diff.String())
			//Append time to sell and buy to result(id).

			rID = append(rID, mapT[tic].ts, mapT[tic].tb)
			result = append(result, rID)
		}
	}

	err = csv.NewWriter(f).WriteAll(result)

	f.Close()

	if err != nil {
		fmt.Println(errors.Wrap(err, "write failed"))
	}
}
func main() {
	rowsU, err := readSample("user_trades.csv")
	if err != nil {
		fmt.Println(err)
	}

	mU := mapUser(rowsU)

	rowsC, err := readSample("candles_5m.csv")
	if err != nil {
		fmt.Println(err)
	}

	mT := mapTicker(rowsC)
	printOutput(mU, mT)
}
